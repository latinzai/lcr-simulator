﻿namespace LCR.Domain
{
    public class LCRPlayer
    {
        public string Name { get; set; }
        public int ChipsCount { get; set; }
        public bool GotChips => ChipsCount > 0;
    }
}
