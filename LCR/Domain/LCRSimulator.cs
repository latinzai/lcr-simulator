﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LCR.Domain.Interfaces;

namespace LCR.Domain
{
    public class LCRSimulator : ILCRSimulator
    {
        public ICollection<ILCRGame> Games { get; set; }

        public int ShortestTurns => Games.Any() ? Games.Min(g => g.Turns) : 0;
        public int LongestTurns => Games.Any() ? Games.Max(g => g.Turns) : 0;
        public double AverageTurns => Games.Any() ? Games.Average(g => g.Turns) : 0;
        private void InitializeGameSet()
        {
            Games = new HashSet<ILCRGame>();
        }
        public LCRSimulator()
        {
            InitializeGameSet();
        }
        public async Task StartSimulation(int playersCount, int gamesCount)
        {
            var tasks = new Task[gamesCount];
            for (int i = 0; i < gamesCount; i++)
            {
                var game = new LCRGame(playersCount);
                Games.Add(game);
                tasks[i] = Task.Factory.StartNew(game.StartGame);
            }

            await Task.WhenAll(tasks);
        }

        public async Task StartNewSimulation(int playersCount, int gamesCount)
        {
            InitializeGameSet();
            await StartSimulation(playersCount, gamesCount);
        }
    }
}
