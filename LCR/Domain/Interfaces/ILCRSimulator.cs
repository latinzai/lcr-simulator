﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace LCR.Domain.Interfaces
{
    public interface ILCRSimulator
    {
        public int ShortestTurns { get; }
        public int LongestTurns { get; }
        public double AverageTurns { get; }
        public ICollection<ILCRGame> Games { get; set; }
        public Task StartSimulation(int playersCount, int gamesCount);
        public Task StartNewSimulation(int playersCount, int gamesCount);
    }
}
