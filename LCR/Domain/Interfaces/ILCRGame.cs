﻿namespace LCR.Domain.Interfaces
{
    public interface ILCRGame
    {
        public void StartGame();

        public int PlayersCount { get; }
        public int Turns { get; set; }
    }
}
