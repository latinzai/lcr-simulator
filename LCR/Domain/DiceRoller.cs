﻿using System;
using System.Collections.Generic;

namespace LCR.Domain
{
    public class DiceRoller
    {
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        private static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            {
                return random.Next(min, max);
            }
        }
        public static List<short> RollDices(int diceCount)
        {
            var results = new List<short>();

            for (int i = 0; i < diceCount; i++)
            {
                var randomNumber = (short)RandomNumber(1, 7);
                results.Add(randomNumber);
            }

            return results;
        }
    }
}
