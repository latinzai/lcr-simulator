﻿using DevExpress.Xpo.Logger;
using LCR.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Unity;

namespace LCR.Domain
{
    public class LCRGame : ILCRGame
    {
        public Guid GameId { get; set; }
        public LinkedList<LCRPlayer> Players { get; set; }
        public int PlayersCount => Players.Count();
        public int Turns { get; set; }
        public int CenterChipCount { get; set; }

        private readonly ILogger Logger;

        private const int STARTING_CHIPS_COUNT = 3;

        public LCRGame(int playersCount)
        {
            GameId = Guid.NewGuid();
            Players = new LinkedList<LCRPlayer>();
            for (int i = 0; i < playersCount; i++)
            {
                Players.AddLast(new LCRPlayer
                {
                    Name = $"Player {i+1}-{GameId}",
                    ChipsCount = STARTING_CHIPS_COUNT
                });
            }
        }

        public void StartGame()
        {
            do
            {
                foreach (var player in Players)
                {
                    Turns += 1;
                    var currentPlayer = Players.Find(player);
                    var rightPlayer = currentPlayer.Next ?? Players.First;
                    var leftPlayer = currentPlayer.Previous ?? Players.Last;

                    if (currentPlayer.Value.GotChips)
                    {
                        var dicesToRoll = currentPlayer.Value.ChipsCount > 3
                            ? 3
                            : currentPlayer.Value.ChipsCount;

                        var results = DiceRoller.RollDices(dicesToRoll);
                        // Logger.Log($"{currentPlayer.Value.Name} got the following dice values: [{string.Join(", ", results)}]!\n");
                        for (int i = 0; i < results.Count; i++)
                        {
                            switch (results[i])
                            {
                                case 4:
                                    PassChipToPlayer(currentPlayer, leftPlayer);
                                    break;
                                case 5:
                                    PassChipToCenter(currentPlayer);
                                    break;
                                case 6:
                                    PassChipToPlayer(currentPlayer, rightPlayer);
                                    break;
                                default:
                                    break;
                            }

                        }
                        
                        // Logger.Log("{0} has {1} chips.\n", player.Name, player.ChipsCount);
                    }
                    else
                    {
                        // Logger.Log("{0} has no chips. passing over to {1}...\n", currentPlayer.Value.Name, rightPlayer.Value.Name);
                    }

                    if (Players.Where(c => c.ChipsCount > 0).Count() == 1)
                    {
                        break;
                    }
                }
            }
            while (Players.Where(c => c.ChipsCount > 0).Count() != 1);

            var winner = Players.Single(c => c.ChipsCount > 0);

            // Logger.Log($"THE WINNER IS {winner.Name}!!!\n");
            // Logger.Log($"Chips in the center: {CenterChipCount}");
            // Logger.Log($"Turns count: {Turns}");
        }

        private void PassChipToCenter(LinkedListNode<LCRPlayer> currentPlayer)
        {
            if (currentPlayer.Value.GotChips)
            {
                // Logger.Log($"{currentPlayer.Value.Name} is passing one chip to the center!\n");
                currentPlayer.Value.ChipsCount -= 1;
                IncreaseCenterChipCount();
            }
        }

        private void IncreaseCenterChipCount()
        {
            CenterChipCount += 1;
        }

        private void PassChipToPlayer(LinkedListNode<LCRPlayer> currentPlayer, LinkedListNode<LCRPlayer> otherPlayer)
        {
            if (currentPlayer.Value.GotChips)
            {
                // Logger.Log($"{currentPlayer.Value.Name} is passing one chip to {otherPlayer.Value.Name}!\n");
                currentPlayer.Value.ChipsCount -= 1;
                otherPlayer.Value.ChipsCount += 1;
            }
        }
    }
}
