﻿using LCR.Domain;
using LCR.Domain.Interfaces;
using Microsoft.Expression.Interactivity.Core;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Windows.Input;
using Unity;

namespace LCR.ViewModels
{
    public class LCRSimulatorViewModel : ViewModelBase
    {
        public ILCRSimulator Simulator { get; set; }
        public LCRSimulatorViewModel()
        {
            var container = new UnityContainer();
            container.RegisterSingleton<ILCRSimulator, LCRSimulator>();
            Simulator = container.Resolve<ILCRSimulator>();
        }

        private int _numberOfPlayers;
        private int _numberOfGames;

        [Required]
        [Range(3, int.MaxValue, ErrorMessage = "You need at least 3 players to play this game.")]
        public int NumberOfPlayers
        {
            get { return _numberOfPlayers; }
            set
            {
                _numberOfPlayers = value;
                NotifyPropertyChanged("NumberOfPlayers");
                NotifyPropertyChanged("ImValid");
            }
        }


        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "You need at least one game to simulate.")]
        public int NumberOfGames
        {
            get { return _numberOfGames; }
            set
            {
                _numberOfGames = value;
                NotifyPropertyChanged("NumberOfGames");
                NotifyPropertyChanged("ImValid");
            }
        }

        public double AverageTurns => Math.Round(Simulator.AverageTurns);
        public int LongestGame => Simulator.LongestTurns;
        public int ShortestGame => Simulator.ShortestTurns;

        private bool _notBusy = true;

        public bool NotBusy
        {
            get { return _notBusy; }
            set { _notBusy = value; NotifyPropertyChanged("NotBusy"); }
        }


        public ICommand RunSimulationCommand => new ActionCommand(async c => await RunSimulation());

        private async Task RunSimulation()
        {
            NotBusy = false;
            await Simulator.StartNewSimulation(NumberOfPlayers, NumberOfGames);
            NotifyPropertyChanged("AverageTurns");
            NotifyPropertyChanged("LongestGame");
            NotifyPropertyChanged("ShortestGame");
            NotBusy = true;
        }
    }
}
