﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;

namespace LCR.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged, IDataErrorInfo
    {
        public string this[string columnName] => OnValidate(columnName);

        public bool ImValid { get; set; }
        public string Error => throw new NotImplementedException();

        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        protected virtual string OnValidate(string propertyName)
        {
         var errorMessage = string.Empty;

            var context = new ValidationContext(this)
            {
                MemberName = propertyName
            };

            var results = new Collection<ValidationResult>();
            this.ImValid = Validator.TryValidateObject(this, context, results, true);
            PropertyChanged(this, new PropertyChangedEventArgs("ImValid"));

            if (!ImValid)
            {
                ValidationResult result = results
                                            .FirstOrDefault(p =>
                                                p.MemberNames.Any(memberName =>
                                                memberName == propertyName));
                errorMessage = result?.ErrorMessage;

            }
            return errorMessage;
        }

        protected void NotifyPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
