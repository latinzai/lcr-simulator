﻿using System;
using System.Diagnostics;
using System.Linq;
using LCR.Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LCR.Test
{
    [TestClass]
    public class LCRTest
    {
        [TestMethod]
        public void Is_Everything_Working_Fine()
        {
            var simulator = new LCRSimulator();
            simulator.StartNewSimulation(10, 100000).Wait();
            Debug.WriteLine(simulator.AverageTurns);
            Debug.WriteLine(simulator.LongestTurns);
            Debug.WriteLine(simulator.ShortestTurns);

            Assert.IsTrue(true);
        }

        [TestMethod]
        public void Does_Result_Count_Matches_Dice_Count()
        {
            const short diceCount = 5;
            var results = DiceRoller.RollDices(diceCount);
            Assert.AreEqual(diceCount, results.Count);
        }

        [TestMethod]
        public void Are_Results_Greater_Than_Zero()
        {
            const short diceCount = 1000;
            var results = DiceRoller.RollDices(diceCount);
            Assert.IsTrue(results.All(c => c > 0));
        }

        [TestMethod]
        public void Are_Results_Less_Than_Seven()
        {
            const short diceCount = 1000;
            var results = DiceRoller.RollDices(diceCount);
            Assert.IsTrue(results.All(c => c < 7));
        }
    }
}
